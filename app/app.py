from flask import Flask
from rediscluster import RedisCluster

app = Flask(__name__)

@app.route('/')
def hello_world():
    print("In hello world")
    startup_nodes = [{"host": "node", "port": "6379"}]
    cluster = RedisCluster(startup_nodes=startup_nodes, decode_responses=True)
    cluster.incr("counter")
    return 'This page has been reached ' + cluster.get('counter') + ' times!!!'


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
