# Docker-Compose Redis Cluster Example

My internet searches failed to find a good example of someone creating a Redis cluster using docker-compose, so I spent far too long getting this working on my own to not leave an example for the next traveler.

## Running
* Can be ran using the run.sh script, or running the docker-compose command within that file
* Application can than be accessed by going to `localhost:5000`
  * Refreshing the screen will show the counter on the page increasing
