# Defining the project name makes it so that the nodes aren't named based on the parent directory
docker-compose -p redis-example up --build -V --scale node=6
